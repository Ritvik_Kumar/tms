package com.example.tms.model;

public class PostModel {

    private final String email;
    private final String password;

    public PostModel(String username, String password) {
        this.email = username;
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }
}