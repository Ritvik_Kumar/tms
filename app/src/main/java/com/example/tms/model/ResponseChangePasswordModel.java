
package com.example.tms.model;

public class ResponseChangePasswordModel {

    private int statusCode;
    private String message;

    private Data data;

    public int getStatusCode() {
        return statusCode;
    }

    public String getMessage() {
        return message;
    }

    public Data getData() {
        return data;
    }

    public class Data{

        private String message;

        public String getMessage() {
            return message;
        }
    }
}