package com.example.tms.model;

public class PostSignUpDataModel {

    private String name;
    private String email;
    private String password;
    private String type;

    public PostSignUpDataModel(String name, String email, String password, String type) {
        this.name = name;
        this.email = email;
        this.password = password;
        this.type = type;
    }
}