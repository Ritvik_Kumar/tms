package com.example.tms.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UserResponse {

//    {
//        "_id":"60894a85360d45001556335d",
//            "title":"abc",
//            "description":"abcdeff",
//            "userId":"60894a6d360d45001556335b",
//            "startDate":12012021,
//            "endDate":13052021,
//            "attachment":"122233dd",
//            "creationDate":"2021-04-28T11:44:05.698Z",
//            "insertDate":1619610246,
//            "managerId":"606de08856708c4a362ebf79",
//            "__v":0
//    }

    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("userId")
    @Expose
    private String userId;
    @SerializedName("creationDate")
    @Expose
    private String creationDate;
    @SerializedName("insertDate")
    @Expose
    private Integer insertDate;
    @SerializedName("managerId")
    @Expose
    private String managerId;
    @SerializedName("__v")
    @Expose
    private String startDate;
    @SerializedName("startDate")
    @Expose
    private String endDate;
    @SerializedName("endDate")
    @Expose
    private Integer v;
    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }
    public String getTitle() {
        return title;
    }
    public void setTitle(String title) {
        this.title = title;
    }
    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }
    public String getUserId() {
        return userId;
    }
    public void setUserId(String userId) {
        this.userId = userId;
    }
    public String getCreationDate() {
        return creationDate;
    }
    public void setCreationDate(String creationDate) {
        this.creationDate = creationDate;
    }
    public Integer getInsertDate() {
        return insertDate;
    }
    public void setInsertDate(Integer insertDate) {
        this.insertDate = insertDate;
    }
    public String getManagerId() {
        return managerId;
    }
    public void setManagerId(String managerId) {
        this.managerId = managerId;
    }
    public Integer getV() {
        return v;
    }
    public void setV(Integer v) {
        this.v = v;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

        public void setEndDate(String endDate) {
        this.endDate = endDate;
    }
}
