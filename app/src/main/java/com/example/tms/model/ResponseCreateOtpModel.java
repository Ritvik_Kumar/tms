package com.example.tms.model;

import java.util.Date;

public class ResponseCreateOtpModel {

    private int statusCode;
    private String message;
    private Data data;
    private Otp otp;

    public int getStatusCode() {
        return statusCode;
    }

    public String getMessage() {
        return message;
    }

    public Data getData() {
        return data;
    }

    public Otp getOtp() {
        return otp;
    }

    public class Data{
        private String message;

        public String getMessage() {
            return message;
        }
    }

    public class Otp {

        private boolean status;
        private int verifyCount;
        private String _id;
        private String email;
        private String type;
        private Date otpExpiry;
        private Date insertData;
        private String otp;
        private String __v;

        public boolean isStatus() {
            return status;
        }

        public int getVerifyCount() {
            return verifyCount;
        }

        public String get_id() {
            return _id;
        }

        public String getEmail() {
            return email;
        }

        public String getType() {
            return type;
        }

        public Date getOtpExpiry() {
            return otpExpiry;
        }

        public Date getInsertData() {
            return insertData;
        }

        public String getOtpData() {
            return otp;
        }

        public String get__v() {
            return __v;
        }
    }
}