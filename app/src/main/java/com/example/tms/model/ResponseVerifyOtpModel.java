package com.example.tms.model;

public class ResponseVerifyOtpModel {
    private int statusCode;
    private String message;
    private Data data;

    public int getStatusCode() {
        return statusCode;
    }

    public String getMessage() {
        return message;
    }

    public Data getData() {
        return data;
    }

    public class Data{
        private int token;
        private String type;

        public int getToken() {
            return token;
        }

        public String getType() {
            return type;
        }
    }
}