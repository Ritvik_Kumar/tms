package com.example.tms.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class ResponseSelectUsersModel {

//     "designation": "MANAGER",
//             "name": "manjit",
//             "email": "ms806909@gmail.com",
//             "password": "$2b$10$oPd2gJERglqNiWXf3sYwIu5yYPtYuJpU75Y0V6he2PsqrJkDsJXLi",
//             "deviceToken": "",
//             "profilePic": "",
//             "_id": "60351bccf4476551e611cf87",
//             "employeeId": "123456789",
//             "type": "iOS developer",
//             "__v": 0

    @SerializedName("apiId")
    @Expose
    private String apiId;

    @SerializedName("statusCode")
    @Expose
    private Integer statusCode;

    @SerializedName("message")
    @Expose
    private String message;

    @SerializedName("data")
    @Expose
    private Data data;

    public String getApiId() {
        return apiId;
    }

    public void setApiId(String apiId) {
        this.apiId = apiId;
    }

    public Integer getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(Integer statusCode) {
        this.statusCode = statusCode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }


    public class Data {

        @SerializedName("taskList")
        @Expose
        private ArrayList<Task> taskList=null;

        public ArrayList<Task> getTaskList() {
            return taskList;
        }

        public void setTaskList(ArrayList<Task> taskList) {
            this.taskList = taskList;
        }
    }

    public class Task {

        @SerializedName("_id")
        @Expose
        private String id;

        @SerializedName("name")
        private String name;

        @SerializedName("__v")
        @Expose
        private Integer v;

        @SerializedName("type")
        @Expose
        private String type;

        @SerializedName("designation")
        @Expose
        private String designation;

        @SerializedName("employeeId")
        @Expose
        private String employeeId;

        @SerializedName("email")
        @Expose
        private String email;


        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public Integer getV() {
            return v;
        }

        public void setV(Integer v) {
            this.v = v;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getDesignation() {
            return designation;
        }

        public void setDesignation(String designation) {
            this.designation = designation;
        }

        public String getEmployeeId() {
            return employeeId;
        }

        public void setEmployeeId(String employeeId) {
            this.employeeId = employeeId;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }
    }
}

