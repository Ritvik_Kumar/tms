package com.example.tms.model;

public class PostChangePasswordModel {

    private String email;
    private int otpToken;
    private String newPassword;

    public PostChangePasswordModel(String email, int otpToken, String newPassword) {
        this.email = email;
        this.otpToken = otpToken;
        this.newPassword = newPassword;
    }
}