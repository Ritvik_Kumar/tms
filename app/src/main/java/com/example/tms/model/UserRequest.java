package com.example.tms.model;

public class UserRequest {

//    {"title":"abc",
//            "description":"abcdeff",
//            "userId":"60894a6d360d45001556335b",
//            "startDate":"12012021",
//            "endDate":"13052021",
//            "attachment":"122233dd"}

    private String title;
    private String description;
    private String userId;
    private String endDate;
    private String startDate;
    private String attachment;

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getAttachment() {
        return attachment;
    }

    public void setAttachment(String attachment) {
        this.attachment = attachment;
    }

    public String getTitle() { return title; }

    public String getDescription() {
        return description;
    }

    public String getUserId() {
        return userId;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}
