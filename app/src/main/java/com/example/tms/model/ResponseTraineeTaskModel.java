package com.example.tms.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class ResponseTraineeTaskModel {

    @SerializedName("apiId")
    @Expose
    private String apiId;

    @SerializedName("statusCode")
    @Expose
    private Integer statusCode;

    @SerializedName("message")
    @Expose
    private String message;

    @SerializedName("data")
    @Expose
    private Data data;

    public String getApiId() {
        return apiId;
    }

    public void setApiId(String apiId) {
        this.apiId = apiId;
    }

    public Integer getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(Integer statusCode) {
        this.statusCode = statusCode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public class Data {

        @SerializedName("taskList")
        @Expose
        private ArrayList<Task> taskList = null;

        public ArrayList<Task> getTaskList() {
            return taskList;
        }

        public void setTaskList(ArrayList<Task> taskList) {
            this.taskList = taskList;
        }
    }

    public class Task {

        @SerializedName("_id")
        @Expose
        private String id;

        @SerializedName("title")
        @Expose
        private String title;

        @SerializedName("description")
        @Expose
        private String description;

        @SerializedName("userId")
        @Expose
        private String userId;

        @SerializedName("creationDate")
        @Expose
        private String creationDate;

        @SerializedName("insertDate")
        @Expose
        private Integer insertDate;

        @SerializedName("managerId")
        @Expose
        private String managerId;

        @SerializedName("__v")
        @Expose
        private Integer v;

        @SerializedName("managerName")
        @Expose
        private String managerName;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getUserId() {
            return userId;
        }

        public void setUserId(String userId) {
            this.userId = userId;
        }

        public String getCreationDate() {
            return creationDate;
        }

        public void setCreationDate(String creationDate) {
            this.creationDate = creationDate;
        }

        public Integer getInsertDate() {
            return insertDate;
        }

        public void setInsertDate(Integer insertDate) {
            this.insertDate = insertDate;
        }

        public String getManagerId() {
            return managerId;
        }

        public void setManagerId(String managerId) {
            this.managerId = managerId;
        }

        public Integer getV() {
            return v;
        }

        public void setV(Integer v) {
            this.v = v;
        }

        public String getManagerName() {
            return managerName;
        }

        public void setManagerName(String managerName) {
            this.managerName = managerName;
        }
    }
}

