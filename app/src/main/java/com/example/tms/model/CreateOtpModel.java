package com.example.tms.model;

public class CreateOtpModel {

    private String email;
    private String type;

    public CreateOtpModel(String email, String type) {
        this.email = email;
        this.type = type;
    }
}