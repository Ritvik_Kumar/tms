package com.example.tms.model;

import java.util.Date;

public class ResponseCreateManagerModel {

    private int statusCode;

    private String message;

    private Data data;

    public int getStatusCode() {
        return statusCode;
    }

    public String getMessage() {
        return message;
    }

    public Data getData() {
        return data;
    }

    public class Data{

        private Manager manager;

        public Manager getManager() {
            return manager;
        }
    }

    public class Manager{

        private String accessToken;

        private String deviceToken;

        private String password;

        private String email;

        private String _id;

        private String name;

        private Date creationDate;

        private int insertDate;

        private int __v;

        public String getAccessToken() {
            return accessToken;
        }

        public String getDeviceToken() {
            return deviceToken;
        }

        public String getPassword() {
            return password;
        }

        public String getEmail() {
            return email;
        }

        public String get_id() {
            return _id;
        }

        public String getName() {
            return name;
        }

        public Date getCreationDate() {
            return creationDate;
        }

        public int getInsertDate() {
            return insertDate;
        }

        public int get__v() {
            return __v;
        }
    }
}