package com.example.tms.model;

public class PostVerifyOtpModel {

    private String email;
    private String type;
    private int otp;

    public PostVerifyOtpModel(String email, String type, int otp) {
        this.email = email;
        this.type = type;
        this.otp = otp;
    }
}