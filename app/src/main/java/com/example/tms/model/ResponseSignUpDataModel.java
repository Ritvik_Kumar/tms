package com.example.tms.model;


public class ResponseSignUpDataModel {

    private int statusCode;
    private String status;
    private Data data;

    public int getStatusCode() {
        return statusCode;
    }

    public String getStatus() {
        return status;
    }

    public Data getData() {
        return data;
    }

    public class Data {

        private String message;

        private UserData userData;

        public String getMessage() { return message; }

        public UserData getUserData() {
            return userData;
        }
    }

    public class UserData {

        private String designation;
        private String name;
        private String email;
        private String password;
        private String _id;
        private String type;
        private String employeeId;

        public String getDesignation() {
            return designation;
        }

        public String getName() {
            return name;
        }

        public String getEmail() {
            return email;
        }

        public String getPassword() {
            return password;
        }

        public String get_id() {
            return _id;
        }

        public String getType() {
            return type;
        }

        public String getEmployeeId() { return employeeId; }
    }
}