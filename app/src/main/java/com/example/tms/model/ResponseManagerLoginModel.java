package com.example.tms.model;

import java.util.Date;

public class ResponseManagerLoginModel {

    private int statusCode;

    private String message;

    private Data data;

    public int getStatusCode() {
        return statusCode;
    }

    public String getMessage() {
        return message;
    }

    public Data getData() {
        return data;
    }

    public class Data {

        private String message;

        private Manager manager;

        public String getMessage() { return message; }

        public Manager getManager() {
            return manager;
        }

        public class Manager{

            private String accessToken;

            private String password;

            private String designation;

            private String email;

            private String _id;

            private String name;

            private String employeeId;

            private String type;

            private Date creationDate;

            private int insertionDate;

            private int __v;

            public String getAccessToken() {
                return accessToken;
            }

            public String getPassword() {
                return password;
            }

            public String getDesignation() {
                return designation;
            }

            public String getEmail() {
                return email;
            }

            public String get_id() {
                return _id;
            }

            public String getName() {
                return name;
            }

            public String getEmployeeId() {
                return employeeId;
            }

            public String getType() {
                return type;
            }

            public Date getCreationDate() {
                return creationDate;
            }

            public int getInsertionDate() {
                return insertionDate;
            }

            public int get__v() {
                return __v;
            }
        }

    }
}