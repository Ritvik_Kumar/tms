package com.example.tms.model;

public class PostSignUpDataManagerModel {
    private String name;
    private String email;
    private String password;
    private String type;
    private String employeeId;


    public PostSignUpDataManagerModel(String name, String email, String password, String type, String employeeId) {
        this.name = name;
        this.email = email;
        this.password = password;
        this.type = type;
        this.employeeId = employeeId;
    }
}
