package com.example.tms.helper;

import org.jetbrains.annotations.NotNull;

public class CommonHelper {

    public boolean isValidUsername(@NotNull String username)
    {
        String regex = "^[A-Za-z]+\\w{4,14}$";
        return username.matches(regex);
    }

    public static boolean isValidEmail(@NotNull String email)
    {
        String regex = "^[\\w-_\\.+]*[\\w-_\\.]\\@([\\w]+\\.)+[\\w]+[\\w]$";
        return email.matches(regex);
    }

    public static boolean isValidPassword(@NotNull String password)
    {
        String regex = "^(?=.*[0-9])"
                + "(?=.*[a-z])(?=.*[A-Z])"
                + "(?=.*[@#$%^&+=])"
                + "(?=\\S+$).{8,14}$";

        return password.matches(regex);
    }
}