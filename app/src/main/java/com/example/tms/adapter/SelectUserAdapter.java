package com.example.tms.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.example.tms.R;
import com.example.tms.model.ResponseSelectUsersModel;

import java.text.BreakIterator;
import java.util.ArrayList;

public class SelectUserAdapter extends RecyclerView.Adapter<SelectUserAdapter.MyViewHolder> {


    Context context;
    View view;
    ArrayList<ResponseSelectUsersModel> tasklist;

    public SelectUserAdapter(Context context) {
        this.context = context;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(context).inflate(R.layout.item_user_detail, parent, false);
        Log.e("LOG_TAG", "My token is" + tasklist);
        return new MyViewHolder(view);

    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {

//        holder.name.setText(tasklist.get(position).getName());
//        holder.email.setText(tasklist.get(position).getEmail());

    }

    @Override
    public int getItemCount() {
        return 10;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView name, email;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.profileName);
            email = itemView.findViewById(R.id.profile_email);

        }
    }
}