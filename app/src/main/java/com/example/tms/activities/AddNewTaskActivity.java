package com.example.tms.activities;
import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import android.view.View;

import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentTransaction;

import com.example.tms.R;
import com.example.tms.adapter.SelectUserAdapter;
import com.example.tms.fragment.SelectUsersFragement;
import com.example.tms.model.UserRequest;
import com.example.tms.model.UserResponse;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddNewTaskActivity extends AppCompatActivity {

    EditText dateFormatStart,dateFormatEnd;
    TextView projectTitle,projectDetails;
    Button next;
    int year,month,date;
    Bundle bundle;
    String token;
    EditText attachable,startDate,endDate;
    Intent myfileintent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_new_task);
        bundle = getIntent().getExtras();
        if (bundle != null) {
            token = bundle.getString("token");}
        Log.e("token for task activity", "My token is" + token);

        view();

        Calendar calender=Calendar.getInstance();

        //onClickListner for datePicker
        dateFormatStart.setOnClickListener(v -> {
            year= calender.get(Calendar.YEAR);
            month= calender.get(Calendar.MONTH);
            date= calender.get(Calendar.DATE);
            DatePickerDialog datePickerDialog=new DatePickerDialog(AddNewTaskActivity.this,
                    (view, year, month, dayOfMonth) ->
                            dateFormatStart.setText(SimpleDateFormat.getDateInstance().format(calender.getTime())),
                    year,month,date);
            datePickerDialog.show();
        });
        dateFormatEnd.setOnClickListener(v -> {
            year= calender.get(Calendar.YEAR);
            month= calender.get(Calendar.MONTH);
            date= calender.get(Calendar.DATE);
            DatePickerDialog datePickerDialog=new DatePickerDialog(AddNewTaskActivity.this,
                    (view, year, month, dayOfMonth) ->
                            dateFormatEnd.setText(SimpleDateFormat.getDateInstance().format(calender.getTime())),
                    year,month,date);
            datePickerDialog.show();
        });



        //onclicklistner for attachable
        attachable.setOnClickListener(v -> {
            myfileintent=new Intent(Intent.ACTION_GET_CONTENT);
            myfileintent.setType("*/*");
            startActivityForResult(myfileintent,10);
        });

        //onclicklistner for next(button
        next.setOnClickListener(v ->
        {assignTask(createRequest());
        switchActivity();} );
    }

    public UserRequest createRequest()
    {
        UserRequest userRequest=new UserRequest();
        userRequest.setDescription(projectDetails.getText().toString());
        userRequest.setTitle(projectTitle.getText().toString());
        userRequest.setStartDate(startDate.getText().toString());
        userRequest.setEndDate(endDate.getText().toString());
        userRequest.setAttachment(attachable.getText().toString());
        return userRequest;
    }

    public void assignTask(UserRequest userRequest){
        Call<UserResponse> userResponseCall=ApiClient.getUserService().assignTask(token,userRequest);
        userResponseCall.enqueue(new Callback<UserResponse>() {
            @Override
            public void onResponse(Call<UserResponse> call, Response<UserResponse> response) {
                if(response.isSuccessful()){
                    Toast.makeText(AddNewTaskActivity.this,"Saved Successfull",Toast.LENGTH_LONG).show();
                }
                else
                {
                    Toast.makeText(AddNewTaskActivity.this,"Access failed",Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<UserResponse> call, Throwable t) {
                Toast.makeText(AddNewTaskActivity.this,"Request Failed" +t.getLocalizedMessage(),Toast.LENGTH_LONG).show();

            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode != 10) {
            return;
        }
        if (resultCode == RESULT_OK) {
            String path = data.getData().getPath();
            attachable.setText(path);
        }
    }

    public void view(){
        //view
        projectTitle=findViewById(R.id.tvProjectTitle);
        projectDetails=findViewById(R.id.tvProjectDetails);
        dateFormatStart=findViewById(R.id.etStartDate);
        dateFormatEnd=findViewById(R.id.etEndDate);
        startDate=findViewById(R.id.etStartDate);
        endDate=findViewById(R.id.etEndDate);
        attachable =findViewById(R.id.etAttachments);
        next=findViewById(R.id.btnNext);
    }

    public void switchActivity() {
        startActivity(new Intent(AddNewTaskActivity.this, SelectUsersActivity.class));

        finish();

    }

}