package com.example.tms.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.example.tms.R;
import com.example.tms.model.PostSignUpDataModel;
import com.example.tms.model.PostSignUpDataManagerModel;
import com.example.tms.model.ResponseCreateManagerModel;
import com.example.tms.model.ResponseSignUpDataModel;
import com.example.tms.service.PostmanApi;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.internal.EverythingIsNonNull;

public class SelectTypeActivity extends AppCompatActivity {

    Button button;
    ImageView imageView;
    String username , password ,email, type , designation, id;
    PostmanApi postmanApi;
    RadioButton iosDeveloper, androidDeveloper, frontEndDeveloper, backEndDeveloper, uiUx;
    RadioGroup radioGroup;
    ResponseCreateManagerModel postResponseManager;
    ResponseSignUpDataModel postResponse;
    ProgressDialog mProgressDialog;
    PostSignUpDataModel post;
    PostSignUpDataManagerModel postManager;
    TextView clientName;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_type);

        setView();

        setOrientation();

        Bundle bundle = getIntent().getExtras();

        if(bundle != null)
        {
            username = bundle.getString("username");
            password = bundle.getString("password");
            email = bundle.getString("email");
            designation = bundle.getString("designation");
            id = bundle.getString("Id");
        }



        clientName.setText(username);
        imageView.setOnClickListener(v -> onBackPressed());

        setupRetrofit();

        button.setOnClickListener(v -> {

            if(radioGroup.getCheckedRadioButtonId() != -1)
            {
                checkType();
                signUpData();
            }
            else
                Toast.makeText(getApplicationContext(),"Please Select a Type",Toast.LENGTH_LONG).show();
        });


    }

    private void setupRetrofit() {

        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.level(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://polar-sea-786.herokuapp.com/")
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        postmanApi = retrofit.create(PostmanApi.class);

    }

    public void signUpData()
    {
        if(designation.equals("Trainee"))
        {
            post = new PostSignUpDataModel(username, email, password, type);
            Call<ResponseSignUpDataModel> call = postmanApi.signUpData(post);

            setProgressDialog();
            retrofitCall(call);
        }

        if(designation.equals("Manager"))
        {
            postManager = new PostSignUpDataManagerModel(username, email, password, id , type);
            Call<ResponseCreateManagerModel> call = postmanApi.signUpDataManager(postManager);

            setProgressDialog();
            retrofitCallManager(call);
        }
    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(getApplicationContext() , SignUpActivity.class));
        finish();
    }

    public void checkType()
    {
        if (iosDeveloper.isChecked())
            type = "iOS Developer";

        else if (androidDeveloper.isChecked())
            type = "Android developer";

        else if (frontEndDeveloper.isChecked())
            type = "Front end developer";

        else if (backEndDeveloper.isChecked())
            type = "backend developer";

        else if (uiUx.isChecked())
            type = "UI/UX";
    }

    public void setView()
    {
        button = findViewById(R.id.btn);
        imageView = findViewById(R.id.backArrow);
        iosDeveloper = findViewById(R.id.iosDeveloper);
        androidDeveloper = findViewById(R.id.androidDeveloper);
        frontEndDeveloper = findViewById(R.id.frontEndDeveloper);
        backEndDeveloper = findViewById(R.id.backEndDeveloper);
        uiUx = findViewById(R.id.uiUx);
        radioGroup = findViewById(R.id.radioGroup);
        clientName = findViewById(R.id.clientName);
    }

    public void retrofitCallManager(Call<ResponseCreateManagerModel> call)
    {
        call.enqueue(new Callback<ResponseCreateManagerModel>() {
            @EverythingIsNonNull
            @Override
            public void onResponse(Call<ResponseCreateManagerModel> call, Response<ResponseCreateManagerModel> response) {

                disappearProgressDialog();
                postResponseManager = response.body();

                if(response.isSuccessful() && postResponseManager != null) {

                    Log.d(response.message(), "");

                    int statusCode = postResponseManager.getStatusCode();


                    switch (statusCode) {

                        case 200: {
                            Toast.makeText(getApplicationContext(), postResponseManager.getStatusCode(), Toast.LENGTH_LONG).show();
                            startActivity(new Intent(getApplicationContext(), LoginActivity.class));
                            finish();
                            break;
                        }

                        case 400: {
                            Toast.makeText(getApplicationContext(), "Invalid Request", Toast.LENGTH_LONG).show();
                            break;
                        }

                        case 401: {
                            Toast.makeText(getApplicationContext(), "Unauthorized Request", Toast.LENGTH_LONG).show();
                            break;
                        }

                        case 404: {
                            Toast.makeText(getApplicationContext(), "Not Found", Toast.LENGTH_LONG).show();
                            break;
                        }

                        case 500: {
                            Toast.makeText(getApplicationContext(), "internet server error", Toast.LENGTH_LONG).show();
                            break;
                        }

                        default: {
                            Toast.makeText(getApplicationContext(), postResponseManager.getStatusCode(), Toast.LENGTH_SHORT).show();
                            break;
                        }

                    }

                }

                else{ Toast.makeText(getApplicationContext(), response.message(), Toast.LENGTH_LONG).show(); }
            }

            @EverythingIsNonNull
            @Override
            public void onFailure(Call<ResponseCreateManagerModel> call, Throwable t) {
                disappearProgressDialog();
                Toast.makeText(getApplicationContext(), "Internet server error", Toast.LENGTH_LONG).show();
            }
        });

    }

    public void retrofitCall(Call<ResponseSignUpDataModel> call)
    {
        call.enqueue(new Callback<ResponseSignUpDataModel>() {
            @EverythingIsNonNull
            @Override
            public void onResponse(Call<ResponseSignUpDataModel> call, Response<ResponseSignUpDataModel> response) {
                disappearProgressDialog();

                postResponse = response.body();

                if(response.isSuccessful() && postResponse != null)
                {
                    int statusCode = postResponse.getStatusCode();


                    switch (statusCode) {

                        case 200: {
                            Toast.makeText(getApplicationContext(), postResponse.getData().getMessage(), Toast.LENGTH_LONG).show();
                            startActivity(new Intent(getApplicationContext(), LoginActivity.class));
                            finish();
                            break;
                        }

                        case 400: {
                            Toast.makeText(getApplicationContext(), "Invalid Request", Toast.LENGTH_LONG).show();
                            break;
                        }

                        case 401: {
                            Toast.makeText(getApplicationContext(), "Unauthorized Request", Toast.LENGTH_LONG).show();
                            break;
                        }

                        case 404: {
                            Toast.makeText(getApplicationContext(), "Not Found", Toast.LENGTH_LONG).show();
                            break;
                        }

                        case 500: {
                            Toast.makeText(getApplicationContext(), "internet server error", Toast.LENGTH_LONG).show();
                            break;
                        }

                        default: {
                            Toast.makeText(getApplicationContext(), postResponse.getStatusCode(), Toast.LENGTH_SHORT).show();
                            break;
                        }

                    }
                }

                else
                {
                    Toast.makeText(getApplicationContext(), postResponse.getData().getMessage(), Toast.LENGTH_SHORT).show();
                }
            }

            @EverythingIsNonNull
            @Override
            public void onFailure(Call<ResponseSignUpDataModel> call, Throwable t) {
                disappearProgressDialog();
                Toast.makeText(getApplicationContext(), "Internet server error", Toast.LENGTH_LONG).show();
            }
        });
    }

    public void setOrientation()
    {
        int currentOrientation = getResources().getConfiguration().orientation;
        if (currentOrientation == Configuration.ORIENTATION_LANDSCAPE) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_PORTRAIT);
        }
    }

    public void setProgressDialog() {
        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.setMessage("Loading...");
        mProgressDialog.show();
    }

    public void disappearProgressDialog()
    {
        if (mProgressDialog.isShowing())
            mProgressDialog.dismiss();
    }
}