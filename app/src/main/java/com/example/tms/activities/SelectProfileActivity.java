package com.example.tms.activities;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.tms.R;

public class SelectProfileActivity extends AppCompatActivity {

    Button button;
    TextView textView;
    RadioButton manager, trainee;
    EditText editText;
    Bundle bundle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_profile);

        setView();

        button.setOnClickListener(v -> {
            if(manager.isChecked() ){
                if(TextUtils.isEmpty(editText.getText().toString()))
                {
                    Toast.makeText(getApplicationContext() , "Enter the Employee Id",Toast.LENGTH_LONG).show();
                }

                else
                {
                    Intent intent = new Intent(getApplicationContext(), SignUpActivity.class);

                    bundle = new Bundle();

                    bundle.putString("designation", "Manager");
                    bundle.putString("Id", editText.getText().toString());

                    intent.putExtras(bundle);
                    startActivity(intent);
                    finish();
                }
            }

            else if(trainee.isChecked())
            {
                Intent intent = new Intent(getApplicationContext(), SignUpActivity.class);

                bundle =new Bundle();

                bundle.putString("designation","Trainee");
                bundle.putString("Id",editText.getText().toString());

                intent.putExtras(bundle);
                startActivity(intent);
                finish();
            }
        });

        textView.setOnClickListener(v -> {
            onBackPressed();
            finish();
        });

        manager.setOnClickListener(v -> editText.setVisibility(View.VISIBLE));

        trainee.setOnClickListener(v -> editText.setVisibility(View.INVISIBLE));

    }

    @Override
    public void onBackPressed()
    {
        startActivity(new Intent(getApplicationContext(), LoginActivity.class));
        finish();
    }

    public void setView()
    {
        button = findViewById(R.id.btn);
        textView = findViewById(R.id.login);
        manager = findViewById(R.id.manager);
        trainee = findViewById(R.id.trainee);
        editText = findViewById(R.id.etProjectDetails);

    }
}