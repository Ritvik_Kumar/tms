package com.example.tms.activities;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.tms.R;
import com.example.tms.helper.CommonHelper;

public class SignUpActivity extends AppCompatActivity {

    Button button;
    Intent intent;
    EditText username, password, email;
    String Username, Password, Email, id;
    String designation;
    Bundle bundle;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        setView();

        bundle = getIntent().getExtras();

        if (bundle!= null)
        {
            designation = bundle.getString("designation");
            id = bundle.getString("Id");
        }

        Log.e("xxxxxxxxxxxxxxxxxx","hsdhjs"+id);

        button.setOnClickListener(v -> {
            Username = username.getText().toString();
            Password = password.getText().toString();
            Email = email.getText().toString();

            validate();

        });
    }

    @Override
    public void onBackPressed()
    {
        startActivity(new Intent(getApplicationContext(), SelectProfileActivity.class));
        finish();
    }

    public void setView()
    {
        button = findViewById(R.id.button);
        username = findViewById(R.id.etUsername);
        password = findViewById(R.id.etPassword);
        email = findViewById(R.id.etEmail);
    }

    public void validate()
    {
        CommonHelper commonHelper = new CommonHelper();

        if(TextUtils.isEmpty(Username))
            Toast.makeText(getApplicationContext(),"Username Missing", Toast.LENGTH_LONG).show();

        else if(!commonHelper.isValidUsername(Username))
            Toast.makeText(getApplicationContext(),"Username must contain at least 5-14 characters eg:- example123", Toast.LENGTH_LONG).show();

        else if(TextUtils.isEmpty(Email))
            Toast.makeText(getApplicationContext(),"Email Missing", Toast.LENGTH_LONG).show();

        else if(!commonHelper.isValidEmail(Email))
            Toast.makeText(getApplicationContext(),"Email format:- abc(.-_)def@mail.com", Toast.LENGTH_LONG).show();

        else if(TextUtils.isEmpty(Password))
            Toast.makeText(getApplicationContext(),"Password Missing", Toast.LENGTH_LONG).show();

        else if(!commonHelper.isValidPassword(Password))
            Toast.makeText(getApplicationContext(),"Password must contain 8-14 characters with one upper Case Character and one special symbol", Toast.LENGTH_LONG).show();

        else{
            Intent intent = new Intent(getApplicationContext(), SelectTypeActivity.class);

            Bundle bundle = new Bundle();

            bundle.putString("username", Username + "");
            bundle.putString("password", Password + "");
            bundle.putString("email", Email + "");
            bundle.putString("designation",designation);
            bundle.putString("Id",id);

            intent.putExtras(bundle);

            startActivity(intent);

            finish();

        }
    }
}