package com.example.tms.activities;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.os.Bundle;
import android.text.TextUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;
import com.example.tms.R;
import com.example.tms.helper.CommonHelper;
import com.example.tms.model.PostChangePasswordModel;
import com.example.tms.model.ResponseChangePasswordModel;
import com.example.tms.service.PostmanApi;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.internal.EverythingIsNonNull;

public class ChangePasswordActivity extends AppCompatActivity {

    ImageView backArrow;
    Bundle bundle;
    int otpToken;
    PostmanApi postmanApi;
    Button button;
    EditText newPassword, confirmPassword;
    String newPasswordString , confirmPasswordString, email;
    ProgressDialog mProgressDialog;
    Call<ResponseChangePasswordModel> call;
    ResponseChangePasswordModel responseChangePasswordModel;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);

        setOrientation();
        setView();

        backArrow.setOnClickListener(view -> onBackPressed());

        bundle = getIntent().getExtras();

        if(bundle != null)
        {
            otpToken = bundle.getInt("otpToken");
            email = bundle.getString("email");
            if(bundle.getString("Msg")!=null){
                Toast.makeText(getApplicationContext(),"data" + bundle.getString("Msg"), Toast.LENGTH_SHORT).show();
            }
        }

        setupRetrofit();

        button.setOnClickListener(view -> newPassword());
    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(getApplicationContext() , ForgotPasswordActivity.class));
        finish();
    }

    public void setOrientation()
    {
        int currentOrientation = getResources().getConfiguration().orientation;
        if (currentOrientation == Configuration.ORIENTATION_LANDSCAPE) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_PORTRAIT);
        }
    }

    public void setView()
    {

        backArrow = findViewById(R.id.backArrowChangePassword);
        button = findViewById(R.id.btnFix);
        newPassword = findViewById(R.id.etNewPassword);
        confirmPassword = findViewById(R.id.etConfirmPassword);

    }

    public void setupRetrofit()
    {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.level(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://polar-sea-786.herokuapp.com/")
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        postmanApi = retrofit.create(PostmanApi.class);

    }

    public void newPassword()
    {
        CommonHelper commonHelper = new CommonHelper();
        newPasswordString = newPassword.getText().toString();
        confirmPasswordString = confirmPassword.getText().toString();

        if(TextUtils.isEmpty(newPasswordString))
            Toast.makeText(getApplicationContext(), "New Password is empty", Toast.LENGTH_LONG).show();

        else if(!commonHelper.isValidPassword(newPasswordString))
            Toast.makeText(getApplicationContext(), "Password must contain 8-14 characters with one upper Case Character and one special symbol", Toast.LENGTH_LONG).show();

        else if(TextUtils.isEmpty(confirmPasswordString))
            Toast.makeText(getApplicationContext(), "Confirm Password is empty", Toast.LENGTH_LONG).show();

        else if(!commonHelper.isValidPassword(confirmPasswordString))
            Toast.makeText(getApplicationContext(), "Password must contain 8-14 characters with one upper Case Character and one special symbol", Toast.LENGTH_LONG).show();

        else if(newPasswordString.equals(confirmPasswordString))
        {
            PostChangePasswordModel postChangePasswordModel = new PostChangePasswordModel(email, otpToken ,newPasswordString);
            call = postmanApi.changePassword(postChangePasswordModel);

            retrofitCall(call);
        }

        else
        {
            Toast.makeText(ChangePasswordActivity.this , "New Password and Confirm Password must be equal",Toast.LENGTH_LONG).show();
        }
    }

    public void retrofitCall(Call<ResponseChangePasswordModel> call)
    {
        setProgressDialog();

        call.enqueue(new Callback<ResponseChangePasswordModel>() {
            @EverythingIsNonNull
            @Override
            public void onResponse(Call<ResponseChangePasswordModel> call, Response<ResponseChangePasswordModel> response) {
                responseChangePasswordModel = response.body();

                if(response.isSuccessful() && responseChangePasswordModel !=null) {

                    disappearProgressDialog();

                    int statusCode = responseChangePasswordModel.getStatusCode();


                    switch (statusCode) {

                        case 200: {
                            Toast.makeText(getApplicationContext(), responseChangePasswordModel.getMessage(), Toast.LENGTH_LONG).show();

                            startActivity(new Intent(getApplicationContext(), LoginActivity.class));
                            finish();

                            break;
                        }

                        case 400: {
                            Toast.makeText(getApplicationContext(), "Invalid Request", Toast.LENGTH_LONG).show();
                            break;
                        }

                        case 401: {
                            Toast.makeText(getApplicationContext(), "Unauthorized Request", Toast.LENGTH_LONG).show();
                            break;
                        }

                        case 404: {
                            Toast.makeText(getApplicationContext(), "Not Found", Toast.LENGTH_LONG).show();
                            break;
                        }

                        case 500: {
                            Toast.makeText(getApplicationContext(), "internet server error", Toast.LENGTH_LONG).show();
                            break;
                        }

                        default: {
                            Toast.makeText(getApplicationContext(), responseChangePasswordModel.getMessage(), Toast.LENGTH_SHORT).show();
                            break;
                        }
                    }
                }

                else
                {
                    Toast.makeText(getApplicationContext(), "Email Address doesn't exist", Toast.LENGTH_LONG).show();
                }


            }

            @EverythingIsNonNull
            @Override
            public void onFailure(Call<ResponseChangePasswordModel> call, Throwable t) {
                disappearProgressDialog();
                Toast.makeText(getApplicationContext(), "Internet server error",Toast.LENGTH_LONG).show();
            }
        });
    }

    public void setProgressDialog()
    {
        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.setMessage("Loading...");
        mProgressDialog.show();
    }

    public void disappearProgressDialog()
    {
        if (mProgressDialog.isShowing())
            mProgressDialog.dismiss();
    }

}