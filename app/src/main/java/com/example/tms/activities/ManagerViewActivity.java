package com.example.tms.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.example.tms.R;
import com.example.tms.fragment.ClockFragment;
import com.example.tms.fragment.HomeFragment;
import com.example.tms.fragment.ReminderFragment;
import com.example.tms.fragment.SettingFragment;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

public class ManagerViewActivity extends AppCompatActivity {

    BottomNavigationView bottomNavigation;
    FloatingActionButton floatbtn;
    Bundle bundle;
    Intent intent;
    String token;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_manager_view);
        bundle = getIntent().getExtras();
        if (bundle != null) {
            token = bundle.getString("token");}
        Log.e("LOG_TAG", "My token is" + token);

        setView();

        floatbtn.setOnClickListener(v -> switchActivity());

        replaceFragment(new HomeFragment(""), "HomeFragment");

        bottomNavigation = findViewById(R.id.bottomNavigation);

        bottomNavigation.setOnNavigationItemSelectedListener(
                item -> {
                    switch (item.getItemId()) {
                        case R.id.menuHome: {

                            replaceFragment(new HomeFragment(""), "HomeFragment");
                            break;
                        }
                        case R.id.menuClock: {

                            replaceFragment(new ClockFragment(), "BlankFragment");
                            break;
                        }

                        case R.id.menuSetting: {
                            replaceFragment(new SettingFragment(""), "SettingFragment");
                            break;
                        }

                        case R.id.menuReminder: {
                            replaceFragment(new ReminderFragment(), "Reminder Fragment");
                            break;
                        }
                    }
                    return true;
                });
    }

    private void replaceFragment(Fragment fragment, String tag) {
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.frameContainer, fragment)
                .commit();
    }

    public void setView()
    {
        floatbtn = findViewById(R.id.floatBtn);
    }


    public void switchActivity()
    {
       intent= new Intent(getApplicationContext() , AddNewTaskActivity.class);
       bundle=new Bundle();
       bundle.putString("token" , token);
       intent.putExtras(bundle);
       startActivity(intent);
       finish();
    }
}