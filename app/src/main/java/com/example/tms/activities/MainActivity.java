package com.example.tms.activities;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import com.example.tms.R;
import com.example.tms.fragment.ClockFragment;
import com.example.tms.fragment.HomeFragment;
import com.example.tms.fragment.ReminderFragment;
import com.example.tms.fragment.SettingFragment;
import com.google.android.material.bottomnavigation.BottomNavigationView;

public class MainActivity extends AppCompatActivity {

    Bundle bundle;
    String token, id;
    BottomNavigationView bottomNavigation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        bundle = getIntent().getExtras();
        if (bundle != null) {
            token = bundle.getString("token");

            id = bundle.getString("id");
        }

        replaceFragment(new HomeFragment(token), "HomeFragment");

        bottomNavigation = findViewById(R.id.bottomNavigation);

        bottomNavigation.setOnNavigationItemSelectedListener(
                item -> {
                    switch (item.getItemId()) {
                        case R.id.menuHome: {
                            replaceFragment(new HomeFragment(token), "HomeFragment");

                            break;
                        }
                        case R.id.menuClock: {
                            replaceFragment(new ClockFragment(), "BlankFragment");

                            break;
                        }

                        case R.id.menuSetting: {
                            replaceFragment(new SettingFragment(id), "SettingFragment");

                            break;
                        }

                        case R.id.menuReminder: {
                            replaceFragment(new ReminderFragment(), "Reminder Fragment");

                            break;
                        }
                    }
                    return true;
                });
    }

    private void replaceFragment(Fragment fragment, String tag) {
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.frameContainer, fragment)
                .commit();
    }


}