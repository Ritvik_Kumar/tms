package com.example.tms.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.tms.R;
import com.example.tms.helper.CommonHelper;
import com.example.tms.model.LoginResponseModel;
import com.example.tms.model.PostModel;
import com.example.tms.model.ResponseManagerLoginModel;
import com.example.tms.service.PostmanApi;
import com.example.tms.service.RetrofitInstance;

import okhttp3.Request;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.internal.EverythingIsNonNull;

public class LoginActivity extends AppCompatActivity {

    TextView tvEmail, tvPassword, tvSignUp, tvForgotPassword;
    PostmanApi postmanApi;
    Button btnLogin;
    ProgressDialog mProgressDialog;
    LoginResponseModel postResponse;
    ResponseManagerLoginModel postManagerResponse;
    Call<LoginResponseModel> call;
    RadioButton rbManager, rbTrainee;
    Call<ResponseManagerLoginModel> callManager;
    Bundle bundle;
    Intent intent;
    String token;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_login_page);

        bundle = new Bundle();

        setView();

        postmanApi = RetrofitInstance.returnRetrofitInstance().getPostmanApi();

        btnLogin.setOnClickListener(v -> createData());

        tvSignUp.setOnClickListener(view -> {
            tvSignUp.setTextColor(getResources().getColor(R.color.orange));

            switchActivity();
        });

        tvForgotPassword.setOnClickListener(view -> forgotPassword());
    }

    public void createData() {
        CommonHelper commonHelper = new CommonHelper();

        if (TextUtils.isEmpty(tvEmail.getText().toString()))
            Toast.makeText(this, "Missing Email", Toast.LENGTH_LONG).show();

        else if (!commonHelper.isValidEmail(tvEmail.getText().toString()))
            Toast.makeText(this, "Email format:- abc(.-_)def@mail.com", Toast.LENGTH_LONG).show();

        else if (TextUtils.isEmpty(tvPassword.getText().toString()))
            Toast.makeText(this, "Missing Password", Toast.LENGTH_LONG).show();

        else if (!commonHelper.isValidPassword(tvPassword.getText().toString()))
            Toast.makeText(this, "Password must contain 8-14 characters with one upper Case Character and one special symbol", Toast.LENGTH_LONG).show();

        else {

            if (rbTrainee.isChecked()) {
                PostModel postModel = new PostModel(tvEmail.getText().toString(), tvPassword.getText().toString());

                call = postmanApi.createData(postModel);

                setProgressDialog();

                retrofitCall(call);
            } else if (rbManager.isChecked()) {
                PostModel postModel = new PostModel(tvEmail.getText().toString(), tvPassword.getText().toString());

                callManager = postmanApi.managerLogin(postModel);

                setProgressDialog();

                retrofitCallManager(callManager);
            } else
                Toast.makeText(this, "Please check an Category", Toast.LENGTH_LONG).show();
        }
    }

    public void retrofitCall(Call<LoginResponseModel> call) {
        call.enqueue(new Callback<LoginResponseModel>() {
            @Override
            @EverythingIsNonNull
            public void onResponse(Call<LoginResponseModel> call, Response<LoginResponseModel> response) {

                disappearProgressDialog();

                postResponse = response.body();

                if (response.isSuccessful() && postResponse != null) {
                    int statusCode = postResponse.getStatusCode();

                    switch (statusCode) {

                        case 200: {
                            Toast.makeText(LoginActivity.this, postResponse.getMessage(), Toast.LENGTH_LONG).show();

                            if (postResponse.getData().getResponse().getDesignation().equals("TRAINEE")) {
                                Intent intent = new Intent(LoginActivity.this, MainActivity.class);

                                bundle.putString("id", postResponse.getData().getResponse().get_id());

                                intent.putExtras(bundle);

                                startActivity(intent);

                                finish();
                            }

                            break;
                        }

                        case 400: {
                            Toast.makeText(LoginActivity.this, "Invalid Request", Toast.LENGTH_LONG).show();

                            break;
                        }

                        case 401: {
                            Toast.makeText(LoginActivity.this, "Unauthorized Request", Toast.LENGTH_LONG).show();

                            break;
                        }

                        case 404: {
                            Toast.makeText(LoginActivity.this, "Not Found", Toast.LENGTH_LONG).show();

                            break;
                        }

                        case 500: {
                            Toast.makeText(LoginActivity.this, "internet server error", Toast.LENGTH_LONG).show();

                            break;
                        }

                        default: {
                            Toast.makeText(LoginActivity.this, postResponse.getMessage(), Toast.LENGTH_SHORT).show();

                            break;
                        }

                    }

                } else
                    Toast.makeText(LoginActivity.this, "Email Address doesn't exist", Toast.LENGTH_LONG).show();

            }

            @Override
            @EverythingIsNonNull
            public void onFailure(Call<LoginResponseModel> call, Throwable t) {
                disappearProgressDialog();
                Toast.makeText(LoginActivity.this, "Internet server error", Toast.LENGTH_LONG).show();
            }
        });
    }

    public void retrofitCallManager(Call<ResponseManagerLoginModel> call) {
        call.enqueue(new Callback<ResponseManagerLoginModel>() {
            @EverythingIsNonNull
            @Override
            public void onResponse(Call<ResponseManagerLoginModel> call, Response<ResponseManagerLoginModel> response) {
                disappearProgressDialog();
                token = response.headers().get("Authorization");
                Log.e("manager first", "My token is" + token);

                postManagerResponse = response.body();

                if (response.isSuccessful() && postManagerResponse != null) {
                    int statusCode = postManagerResponse.getStatusCode();

                    switch (statusCode) {

                        case 200: {
                            Toast.makeText(LoginActivity.this, postManagerResponse.getMessage(), Toast.LENGTH_LONG).show();

                            if (postManagerResponse.getData().getManager().getDesignation().equals("MANAGER")) {
                                intent=new Intent(LoginActivity.this, ManagerViewActivity.class);
                                Log.e("Manager second", "My token is" + token);
                                bundle.putString("token", token);
                                intent.putExtras(bundle);
                                startActivity(intent);
                                finish();
                            }
                            break;
                        }

                        case 400: {
                            Toast.makeText(LoginActivity.this, "Invalid Request", Toast.LENGTH_LONG).show();

                            break;
                        }

                        case 401: {
                            Toast.makeText(LoginActivity.this, "Unauthorized Request", Toast.LENGTH_LONG).show();

                            break;
                        }

                        case 404: {
                            Toast.makeText(LoginActivity.this, "Not Found", Toast.LENGTH_LONG).show();

                            break;
                        }

                        case 500: {
                            Toast.makeText(LoginActivity.this, "internet server error", Toast.LENGTH_LONG).show();

                            break;
                        }

                        default: {
                            Toast.makeText(LoginActivity.this, postManagerResponse.getMessage(), Toast.LENGTH_SHORT).show();

                            break;
                        }

                    }

                } else
                    Toast.makeText(LoginActivity.this, "Invalid Username or Password", Toast.LENGTH_SHORT).show();

            }

            @EverythingIsNonNull
            @Override
            public void onFailure(Call<ResponseManagerLoginModel> call, Throwable t) {
                disappearProgressDialog();
                Toast.makeText(LoginActivity.this, "Internet server error", Toast.LENGTH_LONG).show();
            }
        });
    }

    public void setView() {

        tvEmail = findViewById(R.id.email);

        tvPassword = findViewById(R.id.password);

        btnLogin = findViewById(R.id.btn);

        tvSignUp = findViewById(R.id.signUp);

        tvForgotPassword = findViewById(R.id.tvForgotPassword);

        rbManager = findViewById(R.id.manager);

        rbTrainee = findViewById(R.id.trainee);

    }

    public void switchActivity() {

        startActivity(new Intent(LoginActivity.this, SelectProfileActivity.class));

        finish();

    }

    public void forgotPassword() {

        startActivity(new Intent(LoginActivity.this, ForgotPasswordActivity.class));

        finish();

    }

    public void setProgressDialog() {

        mProgressDialog = new ProgressDialog(this);

        mProgressDialog.setIndeterminate(true);

        mProgressDialog.setMessage("Loading...");

        mProgressDialog.show();

    }

    public void disappearProgressDialog() {
        if (mProgressDialog.isShowing())
            mProgressDialog.dismiss();
    }

}