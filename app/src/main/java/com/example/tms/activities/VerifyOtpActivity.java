package com.example.tms.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Button;
import android.widget.Toast;

import com.example.tms.R;
import com.example.tms.model.PostVerifyOtpModel;
import com.example.tms.model.ResponseVerifyOtpModel;
import com.example.tms.service.PostmanApi;
import com.example.tms.service.RetrofitInstance;
import com.goodiebag.pinview.Pinview;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.internal.EverythingIsNonNull;

public class VerifyOtpActivity extends AppCompatActivity {

    Button btnVerify;
    Bundle bundle;
    Pinview pinview;
    String otp, type, email;
    PostmanApi postmanApi;
    Call<ResponseVerifyOtpModel> call;
    ResponseVerifyOtpModel responseVerifyOtpModel;
    ProgressDialog mProgressDialog;
    Intent intent;
    int otpToken, count = 3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_verify_otp);

        setView();

        postmanApi = RetrofitInstance.returnRetrofitInstance().getPostmanApi();

        bundle = getIntent().getExtras();

        if (bundle != null) {
            otp = bundle.getString("otp");

            email = bundle.getString("email");

            type = bundle.getString("type");
        }

        btnVerify.setOnClickListener(view -> checkOtp());
    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(this, ForgotPasswordActivity.class));

        finish();
    }

    public void setView() {
        pinview = findViewById(R.id.pinView);

        btnVerify = findViewById(R.id.btnVerify);
    }

    public void checkOtp() {
        if (TextUtils.isEmpty(pinview.getValue()))
            Toast.makeText(this, "Please enter Otp", Toast.LENGTH_LONG).show();

        else {
            PostVerifyOtpModel postVerifyOtpModel = new PostVerifyOtpModel(email, type, Integer.parseInt(pinview.getValue()));

            call = postmanApi.verifyOtp(postVerifyOtpModel);

            setProgressDialog();

            retrofitCall(call);
        }
    }

    public void retrofitCall(Call<ResponseVerifyOtpModel> call) {
        call.enqueue(new Callback<ResponseVerifyOtpModel>() {
            @EverythingIsNonNull
            @Override
            public void onResponse(Call<ResponseVerifyOtpModel> call, Response<ResponseVerifyOtpModel> response) {
                disappearProgressDialog();

                responseVerifyOtpModel = response.body();

                if (response.isSuccessful() && responseVerifyOtpModel != null) {
                    int statusCode = responseVerifyOtpModel.getStatusCode();

                    switch (statusCode) {

                        case 200: {
                            Toast.makeText(VerifyOtpActivity.this, responseVerifyOtpModel.getMessage(), Toast.LENGTH_LONG).show();

                            otpToken = responseVerifyOtpModel.getData().getToken();

                            bundle = new Bundle();

                            bundle.putInt("otpToken", otpToken);

                            bundle.putString("email", email);

                            intent = new Intent(VerifyOtpActivity.this, ChangePasswordActivity.class);

                            intent.putExtras(bundle);

                            startActivity(intent);

                            finish();

                            break;
                        }

                        case 400: {
                            Toast.makeText(VerifyOtpActivity.this, "Invalid Request", Toast.LENGTH_LONG).show();

                            break;
                        }

                        case 401: {
                            Toast.makeText(VerifyOtpActivity.this, "Unauthorized Request", Toast.LENGTH_LONG).show();

                            break;
                        }

                        case 404: {
                            Toast.makeText(VerifyOtpActivity.this, "Not Found", Toast.LENGTH_LONG).show();

                            break;
                        }

                        case 500: {
                            Toast.makeText(VerifyOtpActivity.this, "internet server error", Toast.LENGTH_LONG).show();

                            break;
                        }

                        default: {
                            Toast.makeText(VerifyOtpActivity.this, responseVerifyOtpModel.getMessage(), Toast.LENGTH_SHORT).show();

                            break;
                        }

                    }

                } else if (!response.isSuccessful()) {
                    if (count > 0) {
                        Toast.makeText(VerifyOtpActivity.this, "Invalid attempt " + count + " Attempts left", Toast.LENGTH_SHORT).show();

                        count--;
                    } else {
                        Toast.makeText(VerifyOtpActivity.this, "All Attempts failed", Toast.LENGTH_SHORT).show();

                        startActivity(new Intent(VerifyOtpActivity.this, ForgotPasswordActivity.class));

                        finish();
                    }
                } else
                    Toast.makeText(VerifyOtpActivity.this, "Email Address doesn't exist", Toast.LENGTH_LONG).show();
            }

            @EverythingIsNonNull
            @Override
            public void onFailure(Call<ResponseVerifyOtpModel> call, Throwable t) {
                disappearProgressDialog();

                Toast.makeText(VerifyOtpActivity.this, "Internet server error", Toast.LENGTH_LONG).show();
            }
        });
    }

    public void setProgressDialog() {
        mProgressDialog = new ProgressDialog(this);

        mProgressDialog.setIndeterminate(true);

        mProgressDialog.setMessage("Verifying Otp...");

        mProgressDialog.show();
    }

    public void disappearProgressDialog() {
        if (mProgressDialog.isShowing())
            mProgressDialog.dismiss();
    }
}