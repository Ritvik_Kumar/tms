package com.example.tms.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.tms.R;
import com.example.tms.adapter.ReminderAdapter;


public class HomeFragment extends Fragment {

    private final  String token;
    View view;
    ReminderAdapter adapter;
    RecyclerView recyclerView;

    public HomeFragment(String token) {
        this.token= token;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_home, container, false);
        setAdapter(view);
        return view;
    }

    public void setAdapter(View view)
    {
        adapter = new ReminderAdapter(getActivity());
        recyclerView = view.findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setAdapter(adapter);
    }
}