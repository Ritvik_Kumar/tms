package com.example.tms.fragment;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.tms.R;
import com.example.tms.adapter.HomeAdapter;
import com.example.tms.adapter.SelectUserAdapter;
import com.example.tms.model.LoginResponseModel;
import com.example.tms.model.ResponseSelectUsersModel;
import com.example.tms.model.ResponseTraineeTaskModel;
import com.example.tms.service.PostmanApi;
import com.example.tms.service.RetrofitInstance;

import java.util.ArrayList;
import java.util.List;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.internal.EverythingIsNonNull;


public class SelectUsersFragement extends Fragment {
    View view;
    SelectUserAdapter adapter;
    RecyclerView recyclerView;
    PostmanApi postmanApi;
    String token;
    ArrayList<ResponseSelectUsersModel.Task> taskList;

    public SelectUsersFragement(String token) {
        this.token = token;
    }


//   public SelectUserFragement(Activity activity, ArrayList<ResponseTraineeTaskModel.Task> taskList) {
//
//  }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragement_select_users, container, false);

        postmanApi = RetrofitInstance.returnRetrofitInstance().getPostmanApi();

        retrofitCall();

        return view;
    }

    public void setAdapter(View view) {
        adapter = new SelectUserAdapter(getActivity());

        recyclerView = view.findViewById(R.id.recyclerView);

        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        recyclerView.setAdapter(adapter);
    }

    public void retrofitCall() {

        Call<ResponseSelectUsersModel> call = postmanApi.traineeList(token);

        call.enqueue(new Callback<ResponseSelectUsersModel>() {
            @EverythingIsNonNull
            @Override
            public void onResponse(Call<ResponseSelectUsersModel> call, Response<ResponseSelectUsersModel> response) {
                if (response.isSuccessful()) {
                    ResponseSelectUsersModel responseSelectUsersModel = response.body();

                    taskList = responseSelectUsersModel.getData().getTaskList();

                    setAdapter(view);
                }

            }

            @EverythingIsNonNull
            @Override
            public void onFailure(Call<ResponseSelectUsersModel> call, Throwable t) {
                Toast.makeText(getContext(), t.getMessage(), Toast.LENGTH_LONG).show();
            }
        });


    }

}