package com.example.tms.fragment;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.fragment.app.Fragment;

import com.example.tms.R;

import java.io.IOException;
import java.io.InputStream;

public class SettingFragment extends Fragment {

    View view;
    String id, url;
    ImageView userProfile;

    public SettingFragment(String id) {
        this.id = id;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_setting, container, false);

        setProfile(view);

        return view;
    }

    public void setProfile(View view) {
        userProfile = view.findViewById(R.id.icUserDp);

        SharedPreferences preferences = this.getActivity().getSharedPreferences("com.users", Context.MODE_PRIVATE);

        url = preferences.getString(id, "");

        if (!url.equals("")) {
            LoadImage loadImage = new LoadImage(userProfile);

            loadImage.execute(url);
        }
    }

    private class LoadImage extends AsyncTask<String, Void, Bitmap> {
        ImageView userProfile;

        public LoadImage(ImageView userProfile) {
            this.userProfile = userProfile;
        }

        @Override
        protected Bitmap doInBackground(String... strings) {
            String urlLink = strings[0];

            Bitmap bitmap = null;

            try {
                InputStream inputStream = new java.net.URL(urlLink).openStream();

                bitmap = BitmapFactory.decodeStream(inputStream);
            } catch (IOException e) {
                e.printStackTrace();
            }
            return bitmap;
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {
            userProfile.setImageBitmap(bitmap);
        }
    }
}