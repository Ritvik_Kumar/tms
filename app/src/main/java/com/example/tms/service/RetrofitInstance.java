package com.example.tms.service;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitInstance {
    public static RetrofitInstance instance = null;
    public PostmanApi postmanApi;
    public static final String baseUrl = "https://polar-sea-786.herokuapp.com/";

    private RetrofitInstance() {
        buildRetrofit();
    }

    public static RetrofitInstance returnRetrofitInstance() {
        synchronized (RetrofitInstance.class) {
            if (instance == null) {
                instance = new RetrofitInstance();
            }
            return instance;
        }
    }

    public void buildRetrofit()
    {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.level(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(baseUrl)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        postmanApi = retrofit.create(PostmanApi.class);
    }

    public PostmanApi getPostmanApi()
    {
        return this.postmanApi;
    }
}