package com.example.tms.service;

import com.example.tms.model.CreateOtpModel;
import com.example.tms.model.LoginResponseModel;
import com.example.tms.model.PostModel;
import com.example.tms.model.PostChangePasswordModel;
import com.example.tms.model.PostSignUpDataModel;
import com.example.tms.model.PostSignUpDataManagerModel;
import com.example.tms.model.PostVerifyOtpModel;
import com.example.tms.model.ResponseChangePasswordModel;
import com.example.tms.model.ResponseCreateManagerModel;
import com.example.tms.model.ResponseCreateOtpModel;
import com.example.tms.model.ResponseImageModel;
import com.example.tms.model.ResponseManagerLoginModel;
import com.example.tms.model.ResponseSelectUsersModel;
import com.example.tms.model.ResponseSignUpDataModel;
import com.example.tms.model.ResponseTraineeTaskModel;
import com.example.tms.model.ResponseVerifyOtpModel;
import com.example.tms.model.UserRequest;
import com.example.tms.model.UserResponse;

import java.util.List;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

public interface PostmanApi {

    @POST("api/user/login")
    Call<LoginResponseModel> createData(@Body PostModel postModel);

    @POST("api/manager/login")
    Call<ResponseManagerLoginModel> managerLogin(@Body PostModel postModel);

    @POST("api/user/signup")
    Call<ResponseSignUpDataModel> signUpData(@Body PostSignUpDataModel postSignUpDataModel);

    @POST("api/manager/")
    Call<ResponseCreateManagerModel> signUpDataManager(@Body PostSignUpDataManagerModel postSignUpDataManagerModel);

    @POST("api/otp/create")
    Call<ResponseCreateOtpModel> createOtp(@Body CreateOtpModel createOtpModel);

    @POST("api/otp/verify")
    Call<ResponseVerifyOtpModel> verifyOtp(@Body PostVerifyOtpModel postVerifyOtpModel);

    @POST("api/user/password/reset/email")
    Call<ResponseChangePasswordModel> changePassword(@Body PostChangePasswordModel postChangePasswordModel);

    @GET("api/task")
    Call<ResponseSelectUsersModel> traineeList(@Header("Authorization") String token);

//    @Multipart
//    @POST("api/s3upload/image-upload")
//    Call<ResponseImageModel> imageUpload(@Part MultipartBody.Part file);

    @POST("api/task")
    Call<UserResponse> assignTask(@Header("Authorization") String token,@Body UserRequest userRequest);

    @GET("api/user/")
    Call<List<UserResponse>> getAllUsers();

}